%   Written by: Matan Shaked

full_adder(X,Y,Cin,Z,Cout) :- 
    Z is (X+Y+Cin) mod 2,
    Cout is (X+Y+Cin) div 2.

addition_circuit(N,Xs,Ys,Zs,Fs) :-
    add_circuit_aux(N,Xs,Ys,Zs,0,Fs).   % calculate addition circuit with an auxilary 0 Cin

add_circuit_aux(0,[],[],[Cout],Cout,[]).
add_circuit_aux(N,[X|Xs],[Y|Ys],[Z|Zs],Cin,[F|Fs]) :-
    N>0,
    F = full_adder(X,Y,Cin,Z,Cout), % add current bits and curry
    N1 is N-1,
    add_circuit_aux(N1,Xs,Ys,Zs,Cout,Fs).   % send current output curry to the next adder


eval_addition_circuit([full_adder(X,Y,Cin,Z,Cout)|Fs]) :-
     full_adder(X,Y,Cin,Z,Cout), eval_addition_circuit(Fs).
eval_addition_circuit([]).


not_an_addition_circuit(Xs, Ys, Zs, Fs):-
    instantiate_arguments(Xs,Ys),   % create an actual input to the circuit
    eval_addition_circuit(Fs),      % calculate its output
    calc_addition(Xs,Ys,Sum),       % calculate actual addition of the inputs
    not_equal(Sum, Zs).             % test if the actual addition is different from the output of the circuit.

instantiate_arguments([0|Xs], [0|Ys]):- instantiate_arguments(Xs,Ys).
instantiate_arguments([0|Xs], [1|Ys]):- instantiate_arguments(Xs,Ys).
instantiate_arguments([1|Xs], [0|Ys]):- instantiate_arguments(Xs,Ys).
instantiate_arguments([1|Xs], [1|Ys]):- instantiate_arguments(Xs,Ys).
instantiate_arguments([],[]).

circuit_out_not_equal([full_adder(_,_,_,Out,_)|Fs], [Z|Zs]) :-
    Out=Z,
    circuit_out_not_equal(Fs,Zs).
circuit_out_not_equal([full_adder(_,_,_,Out,_)|_], [Z|_]) :-
    Out=\=Z.
circuit_out_not_equal([full_adder(_,_,_,Out,_)], [Z1,_]) :-
    Out =\= Z1.
circuit_out_not_equal([full_adder(_,_,_,_,Cout)], [_,Z2]) :-
    Cout =\= Z2.

calc_addition(Xs,Ys,Sum) :- 
    add_bin(Xs,Ys,0,Sum).
add_bin([X|Xs],[Y|Ys], C, [S| Sum]) :-
    S is (X+Y+C) mod 2,
    Cout is (X+Y+C) div 2,
    add_bin(Xs,Ys,Cout,Sum).
add_bin([],[],1,[1]).
add_bin([],[],0,[]).

not_equal([A|_],[B|_]) :-
    A =\= B.
not_equal([A|As],[B|Bs]) :-
    A = B,
    not_equal(As,Bs).


%   I chose to implement an odd-even merge sort.
sorting_network(N, Cs, In, Out) :-
    N > 2,
    N2 is N/2,
    length(In1,N2), length(In2,N2),
    append(In1,In2,In),
    sorting_network(N2,Cs1,In1,Out1),   % sort left half of the input == get comparators for sorting left half
    sorting_network(N2,Cs2,In2,Out2),   % sort right half of the input == get comparators for sorting right half
    merge_sorted(N2,Out1,Out2,Out,CsMerge), % merge sorted halfs == get comparators for merging two sorted lists
    append(Cs1,Cs2,Cs3),    % combine comparators for sorting each half
    append(Cs3, CsMerge, Cs).   % combine comparators for sorting all inputs
sorting_network(2,[Cs],[In1,In2], [Out1,Out2]) :-
    Cs=comparator(In1,In2,Out1,Out2).


merge_sorted(N, In1, In2, Out, Cs) :-
    append(In1,In2,In),
    odd_even(In,Odd,Even),  % get the lists of evenly indexed items and odd indexed items.
    sorting_network(N, CsSortEven, Even, SortedEven),   % sort evenly indexed items
    sorting_network(N, CsSortOdd, Odd, SortedOdd),      % sort oddly indexed items
    SortedEven=[E|SortedEvenToMerge],   % get the even numbers without the first one
    append(SortedOddToMerge, [O], SortedOdd),   % get the odd numbers without the last one
    merge(SortedEvenToMerge,SortedOddToMerge,Merged,CsMerge),   % merge sorted lists of even and odd indexes
    append([E],Merged,Out1),    % add the first even indexed element back to the sorted list
    append(Out1,[O],Out),       % add the last odd indexed element back to the sorted list
    append(CsSortEven,CsSortOdd,CsHalves),  % combine comparators for sorting each half of the list
    append(CsHalves,CsMerge, Cs).   % combine comparators with the merge comparators

odd_even([A,B|In],[B|Odd],[A|Even]) :-
    odd_even(In,Odd,Even).
odd_even([A], [], [A]).
odd_even([],[],[]).

merge([E|Even],[O|Odd], [Min,Max|Out], [C|Cs]) :-
    C=comparator(E,O,Min,Max),
    merge(Even,Odd,Out,Cs).
merge([],[],[],[]).



measure_network(Cs,In,Out,Depth,Size) :-
    length(Cs,Size),    % First, calculate the size of the network
    depths(Cs,In,Out,[D|Depths]),   % Then, calculate the depths starting from each of the outputs
    max_of_list([D|Depths], D, Depth).  % Finally, calculate the maximal of such lengths, among all outputs.

depths(Cs, In, [O|Out], [D|Depth]) :-
    back_propagate(Cs,In,O,D),
    depths(Cs,In,Out,Depth).
depths(_,_,[],[]).

%   This predicate propagates backwards along the network, and calculates the maximal length of paths which reach "Curr"
back_propagate(Cs,In,Curr,Depth) :-
    member(comparator(In1,In2,Out,_),Cs),
    Out==Curr,  % find comparator whose first output is "Curr"
    back_propagate(Cs,In,In1,Depth1),   % calculate the maximal length of paths which reach the comparator's first input 
    back_propagate(Cs,In,In2,Depth2),   % calculate the maximal length of paths which reach the comparator's second input
    Depth is 1+max(Depth1,Depth2).      % the depth is the maximal among inputs + 1
back_propagate(Cs,In,Curr,Depth) :-
    member(comparator(In1,In2,_,Out),Cs),   
    Out==Curr,  % Same method as before, only now we find the comparator whose second output is "Curr"
    back_propagate(Cs,In,In1,Depth1),
    back_propagate(Cs,In,In2,Depth2),
    Depth is 1+max(Depth1,Depth2).
back_propagate(_,In,Curr,0) :-
    member(X,In),
    X==Curr.    % We terminate upon reaching an input of the network

max_of_list([],Max,Max).
max_of_list([Elem|List], CurrMax, Max) :-
    NextMax is max(Elem,CurrMax),
    max_of_list(List,NextMax,Max).


apply_network([C|Cs],In,Out) :-
    C= comparator(In1,In2,Out1,Out2),
    comparator(In1,In2,Out1,Out2),  % apply current comparator
    apply_network(Cs,In,Out).       % continue recursively
apply_network([],_,_).


comparator(A,B,X,Y) :-
    X is min(A,B),
    Y is max(A,B).



not_a_sorting_network(Cs, In, Out) :-
    binary_list(In),    % generate a binary list
    apply_network(Cs,In,Out),   % calculate the network's output
    sort_binary(In,Sorted),     % calculate actual sorted input
    not_equal(Sorted,Out).      % test that the output of the network is not the sorted list.

binary_list([0|Bin]):-
    binary_list(Bin).
binary_list([1|Bin]):-
    binary_list(Bin).
binary_list([]).

sort_binary(List,Out):-
    zeroes(List,Zeroes),    % count zeroes
    ones(List,Ones),        % count ones
    append(Zeroes,Ones,Out).% concant

zeroes([0|List], [0|Zeroes]):-
    zeroes(List,Zeroes).
zeroes([1|List], Zeroes) :-
    zeroes(List,Zeroes).
zeroes([],[]).

ones([1|List], [1|Ones]):-
    ones(List,Ones).
ones([0|List], Ones) :-
    ones(List,Ones).
ones([],[]).



encode_full_adder(X,Y,Cin,Z,Cout,CNF) :-
    CNF = [
        [-X,-Y,-Cin, Z],
        [X,Y,-Cin, Z],
        [-X,Y,Cin, Z],
        [X,-Y,Cin, Z],
        [-X,Y,-Cin, -Z],
        [-X,-Y,Cin, -Z],
        [X,-Y,-Cin, -Z],
        [X,Y,Cin, -Z],
        [-X,-Y,Cin,Cout],
        [X,-Y,-Cin,Cout],
        [-X,Y,-Cin,Cout],
        [-X,-Y,-Cin,Cout],
        [-X,Y,Cin,-Cout],
        [X,-Y,Cin,-Cout],
        [X,Y,-Cin,-Cout],
        [X,Y,Cin,-Cout]
    ].

eval_cnf([Clause|Cnf]) :-
    or(Clause),
    eval_cnf(Cnf).
eval_cnf([]).

or([1|_]).
or([- 0|_]).
or([0|Clause]) :-
    or(Clause).
or([- 1|Clause]) :-
    or(Clause).


encode_binary_addition(N,Xs,Ys,Zs,CNF) :-
    encode_binary_addition_aux(N,Xs,Ys,Zs,0,CNF).

encode_binary_addition_aux(0,[],[],[Cout],Cout,Cnf) :-
    encode_full_adder(0,0,Cout,Cout,0,Cnf).
encode_binary_addition_aux(N, [X|Xs], [Y|Ys], [Z|Zs], Cin, CNF) :-  % equivalent to the definition of addition circuit with full adders (Q1)
    N>0,
    encode_full_adder(X,Y,Cin,Z,Cout, CurrAdditionCnf),
    append(CurrAdditionCnf, NextCnf, CNF),
    N1 is N-1,
    encode_binary_addition_aux(N1,Xs,Ys,Zs,Cout,NextCnf).