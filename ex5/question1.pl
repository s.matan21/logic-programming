%%%% Template for solving midterm #2 question #1

%%% DECLARATION 

/* Fill in your name and ID
  I, Name (ID number) assert that the work I submitted is entirely my
  own.  I have not received any part from any other student in the
  class (or other source), nor did I give parts of it for use to
  others. I have not taken any code from any external source except
  that which appears on the one page I submitted to the submission
  system before the Midterm
*/

% Part 1
%% knightMove(+,+,-).

knightMove(N, cell(I,J), cell(K,L)) :-
  K is I+2,
  L is J+1,
  within_board(K,L,N).
knightMove(N, cell(I,J), cell(K,L)) :-
  K is I-2,
  L is J+1,
  within_board(K,L,N).
knightMove(N, cell(I,J), cell(K,L)) :-
  K is I+2,
  L is J-1,
  within_board(K,L,N).
knightMove(N, cell(I,J), cell(K,L)) :-
  K is I-2,
  L is J-1,
  within_board(K,L,N).
knightMove(N, cell(I,J), cell(K,L)) :-
  K is I+1,
  L is J+2,
  within_board(K,L,N).
knightMove(N, cell(I,J), cell(K,L)) :-
  K is I-1,
  L is J+2,
  within_board(K,L,N).
knightMove(N, cell(I,J), cell(K,L)) :-
  K is I+1,
  L is J-2,
  within_board(K,L,N).
knightMove(N, cell(I,J), cell(K,L)) :-
  K is I-1,
  L is J-2,
  within_board(K,L,N).

within_board(I,J,N) :-
  between(1,N,I),
  between(1,N,J).


% Part 2
%% allKnightMoves(+,+,-).

allKnightMoves(N,cell(I,J),Moves):-
  findall(cell(K,L), knightMove(N,cell(I,J),cell(K,L)), Moves).


% Part 3
%% multiKnightMoves(+,+,+,-).

multiKnightMoves(0,_,cell(I,J), [cell(I,J)]).
multiKnightMoves(K,N, cell(I,J), Cells) :-
  K>=1,
  allKnightMoves(N,cell(I,J),CurrMoves),
  K1 is K-1,
  findall(
    NextMoves,
    (
      member(NextMovesOrigin,CurrMoves),
      multiKnightMoves(K1,N,NextMovesOrigin,NextMoves)
    ),
    AllMovesNested
  ),
  flatten(AllMovesNested, MaybeDuplicateCells),
  sort(MaybeDuplicateCells,Cells). % deduplicate cells.