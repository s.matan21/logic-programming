%%%% Template for solving midterm #2 question #2

%%% DECLARATION 

/* Fill in your name and ID
  I, Name (ID number) assert that the work I submitted is entirely my
  own.  I have not received any part from any other student in the
  class (or other source), nor did I give parts of it for use to
  others. I have not taken any code from any external source except
  that which appears on the one page I submitted to the submission
  system before the Midterm
*/

% Part 1
%% literal(+,-).

literal(Y,Y=1) :-
  var(Y).
literal(Y,Var=Val) :-
  \+ var(Y),
  countMinus(Y,Var,Amount),
  Parity is Amount mod 2,
  (Parity=0 -> Val=1 ; Val= - 1).

countMinus(X,X,0) :-
  var(X).
countMinus(-X,X,1) :-
  var(X).
countMinus(-X,Var,Amount) :-
  \+ var(X),
  countMinus(X,Var,Amount1),
  Amount is Amount1 + 1.

% Part 2
%% true(+).

true([Literal | _]) :-
  literalTrue(Literal).
true([Literal | Clause]) :-
  \+ literalTrue(Literal),
  true(Clause).

literalTrue(X) :-
  term_variables(X, Vars),
  length(Vars,0),
  evenMinusesForTruthValue(X).

evenMinusesForTruthValue(1).
evenMinusesForTruthValue(-X) :-
  \+ evenMinusesForTruthValue(X).

% Part 3
%% unit(+,-).

unit(Clause,Assignment) :-
  term_variables(Clause, Vars),
  length(Vars,1),
  \+ true(Clause),
  freeVarTerm(Clause, Term),
  literal(Term,Assignment).

% finds the first term in the clause which has a free variable.
freeVarTerm([Term | _], Term) :-
  term_variables(Term,Vars),
  length(Vars,1).
freeVarTerm([Term | Clause], OtherTerm) :-
  term_variables(Term,Vars),
  length(Vars,L),
  L<1,
  freeVarTerm(Clause,OtherTerm).

% Part 4
%% propagate(+,-).

propagate(Cnf1,Cnf2) :-
  existsUnitClause(Cnf1,Unit,Var=Val),
  Var=Val,
  delete(Cnf1,Unit,NewCnf1),
  propagate(NewCnf1,Cnf2).
propagate(Cnf1,Cnf2) :-
  existsTrueClause(Cnf1,TrueClause),
  delete(Cnf1,TrueClause, NewCnf1),
  propagate(NewCnf1,Cnf2).
propagate(Cnf1,Cnf1) :-
  \+ existsUnitClause(Cnf1,_,_),
  \+ existsTrueClause(Cnf1,_).


existsUnitClause([Clause|_], Clause, Assignment) :-
  unit(Clause, Assignment).
existsUnitClause([Clause|Cnf], Unit, Assignment) :-
  \+ unit(Clause,_),
  existsUnitClause(Cnf,Unit,Assignment).

existsTrueClause([Clause|_], Clause) :-
  true(Clause).
existsTrueClause([Clause|Cnf], TrueClause) :-
  \+ true(Clause),
  existsTrueClause(Cnf,TrueClause).

% propagate([],[]).

% propagate([Clause|Cnf1], Cnf2) :- 
%   unit(Clause,Var=Val),
%   Var=Val,
%   trace,
%   propagate(Cnf1,Cnf2).

% propagate([Clause|Cnf1], Cnf2) :- 
%   true(Clause),
%   propagate(Cnf1,Cnf2).

% propagate([Clause|Cnf1], [Clause|Cnf2]) :- 
%   \+unit(Clause,_),
%   \+true(Clause),
%   propagate(Cnf1,Cnf2).