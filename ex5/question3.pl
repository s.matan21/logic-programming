%%%% Template for solving midterm #2 question #3

%%% DECLARATION 

/* Fill in your name and ID
  I, Name (ID number) assert that the work I submitted is entirely my
  own.  I have not received any part from any other student in the
  class (or other source), nor did I give parts of it for use to
  others. I have not taken any code from any external source except
  that which appears on the one page I submitted to the submission
  system before the Midterm
*/

%%% fill in and uncomment next line: where is your sat solver
% :- use_module('.......satsolver/satsolver'),[sat/1]).



% Part 1
%% triplets(+,-).

triplets(N,Ts) :-
  findall(
    (A,B,C), 
    (
      between(1,N,A),
      between(1,N,B),
      A =< B,
      A2 is A*A,
      B2 is B*B,
      Sum is A2+B2,
      Cfloat is sqrt(Sum),
      C is floor(Cfloat),
      C =:= Cfloat,
      B=<C,
      between(1,N,C)
    ),
    Ts
  ).


% Part 2
%% verify(+,+,-).

verify(N,sol(Set1,Set2),Result) :-
  triplets(N,Ts),
  tripletInTheSameSet(Ts,Set1,Set2,Result).
verify(N,sol(Set1,Set2),Result) :-
  notPartition(N,Set1,Set2),
  findall(I,between(1,N,I),Numbers),
  Result = (union(Set1,Set2)\=Numbers).
verify(_,sol(Set1,Set2),Result) :-
  intersectionNotEmpty(Set1,Set2,Intersection),
  Result = intersection(Set1,Set2,Intersection).
  
verify(N,sol(Set1,Set2),ok) :-
  triplets(N,Ts),
  \+ tripletInTheSameSet(Ts,Set1,Set2,_),
  \+ notPartition(N,Set1,Set2),
  \+ intersectionNotEmpty(Set1,Set2,_).

tripletInTheSameSet([(A,B,C)|_], Set1, _, (A,B,C):Set1) :-
  member(A, Set1),
  member(B, Set1),
  member(C, Set1).
tripletInTheSameSet([(A,B,C)|_], _, Set2, (A,B,C):Set2) :-
  member(A, Set2),
  member(B, Set2),
  member(C, Set2).
tripletInTheSameSet([(A,B,C)|Ts], Set1, Set2, Result) :-
  \+ member(A, Set1),
  \+ member(B, Set1),
  \+ member(C, Set1),
  \+ member(A, Set2),
  \+ member(B, Set2),
  \+ member(C, Set2),
  tripletInTheSameSet(Ts,Set1,Set2,Result).

notPartition(N,Set1,Set2) :-
  append(Set1,Set2,All),
  sort(All,Sorted),
  \+ findall(I,between(1,N,I), Sorted).

intersectionNotEmpty(Set1,Set2,Intersection) :-
  intersection(Set1,Set2,Intersection),
  length(Intersection, L),
  L>0.
  


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parts 3 and 4


solve(N,Sol) :-
    encode(N,Map,Cnf),
    (sat(Cnf) -> decode(Map,Sol) ; Sol=unsat),  
    verify(N,Sol,V),
    writeln(verify:V).


% Part 3
%% encode(+,-,-).

%% fill in your code here


% Part 4
%% decode(+,-).

%% fill in your code here


