% Written by: Matan Shaked.

kakuroVerify([Clue=Vars|Instance],[Clue=VarsVals|Solution]) :-
    clue_is_valid(Clue=Vars, Clue=VarsVals),
    kakuroVerify(Instance, Solution).
kakuroVerify([],[]).


clue_is_valid(Clue=[_|Rest],Clue=[CurrValue|RestVals]) :-
    Clue>0,
    Clue1 is Clue-CurrValue,
    clue_is_valid(Clue1=Rest,Clue1=RestVals).
clue_is_valid(0=[],0=[]).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

kakuroEncode(Instance,Instance,Constraints) :-
    deduplicateVars(Instance, DedupedVars),
    declareVars(DedupedVars, VarsDeclarations),
    blocksSumToClue(Instance, Sums),
    eachBlockIsAllDifferent(Instance,Different),
    append(VarsDeclarations, Sums, PartialConstraints),
    append(PartialConstraints,Different, Constraints).

deduplicateVars(Instance, DedupedVars) :-
    allVarsFromBlock(Instance, AllVars),
    term_variables(AllVars, DedupedVars).
allVarsFromBlock([],[]).
allVarsFromBlock([_=Block|Instance],Vars):-
    append(Block,NextVars,Vars),
    allVarsFromBlock(Instance, NextVars).    


declareVars([],[]).
declareVars([Var|Vars], [Decl|VarsDeclarations]) :-
    Decl=new_int(Var,1,9),    % make new prolog variable and pair it to the instance variable.
    declareVars(Vars, VarsDeclarations).

makeBlockVars([],[], []).
makeBlockVars([Var|Block],[Var=Decl | VarsMap], [Decl | Declarations]) :-
    Decl=new_int(Var,1,9),
    makeBlockVars(Block,VarsMap,Declarations).
    
blocksSumToClue([], []).
blocksSumToClue([Val=Block|Instance], [Sum|Sums]) :-
    Sum=int_array_sum_eq(Block,Val),
    blocksSumToClue(Instance, Sums).

eachBlockIsAllDifferent([],[]).
eachBlockIsAllDifferent([_=Block|Instance], [Diff|Differences]) :-
    Diff=int_array_allDiff(Block),
    eachBlockIsAllDifferent(Instance,Differences).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

kakuroDecode([],[]).
kakuroDecode([Val=BlockBits|Map],[Val=BlockValues|Solution]) :-
    bDecode:decodeIntArray(BlockBits, BlockValues),
    kakuroDecode(Map,Solution).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

kakuroSolve(Instance, Solution) :-
    kakuroEncode(Instance, Map, Constraints),
    bCompile(Constraints, Cnf),
    sat(Cnf),
    kakuroDecode(Map,Solution),
    kakuroVerify(Solution,Solution).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

verify_killer(killer(_),Solution) :-
    Solution=Assignments,
    findall(cell(I,J), box_valid(Assignments, I,J), CellsWithValidBoxes),
    findall(cell(I,J), row_valid(Assignments, I,J), CellsWithValidRows),
    findall(cell(I,J), col_valid(Assignments, I,J), CellsWithValidCols),
    findall(cell(I,J), kings_move_valid(Assignments, I,J), CellsWithValidKingsMoves),
    findall(cell(I,J), knights_move_valid(Assignments, I,J), CellsWithValidKnightsMoves),
    findall(cell(I,J), adjacent_cell_valid(Assignments, I,J), CellsWithValidAdjacentCells),
    is_enough_valid_cells(CellsWithValidBoxes, CellsWithValidRows, CellsWithValidCols, CellsWithValidKingsMoves, CellsWithValidKnightsMoves, CellsWithValidAdjacentCells).

row_cell(Assignments, I,J, cell(I,L)=U) :-
    between(1,9,L),
    L \= J,
    member(cell(I,L)=U, Assignments).
col_cell(Assignments, I,J, cell(K,J)=U) :-
    between(1,9,K),
    K \= I,
    member(cell(K,J)=U, Assignments).
box_cell(Assignments, I,J, cell(K,L)=U) :-
    I1 is I-1,
    J1 is J-1,
    BoxI is div(I1,3),
    BoxJ is div(J1,3),
    BoxStartRow is (BoxI+1)*3 - 3+1,
    BoxStartCol is (BoxJ+1)*3 - 3+1,
    between(0,2, RowOffset),
    between(0,2, ColOffset),
    K is BoxStartRow + RowOffset,
    L is BoxStartCol + ColOffset,
    K\=I,
    L\=J,
    member(cell(K,L)=U, Assignments).

adjacent_cell(Assignments, I,J, cell(K,L)=V) :-
    K is I,
    L is J+1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
adjacent_cell(Assignments, I,J, cell(K,L)=V) :-
    K is I,
    L is J-1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
adjacent_cell(Assignments, I,J, cell(K,L)=V) :-
    K is I+1,
    L is J,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
adjacent_cell(Assignments, I,J, cell(K,L)=V) :-
    K is I-1,
    L is J,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).

kings_move(Assignments, I,J, cell(K,L)=V) :-
    adjacent_cell(Assignments,I,J,cell(K,L)=V).
kings_move(Assignments, I,J, cell(K,L)=V) :-
    K is I+1,
    L is J+1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
kings_move(Assignments, I,J, cell(K,L)=V) :-
    K is I+1,
    L is J-1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
kings_move(Assignments, I,J, cell(K,L)=V) :-
    K is I-1,
    L is J+1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
kings_move(Assignments, I,J, cell(K,L)=V) :-
    K is I-1,
    L is J-1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).

knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I+2,
    L is J+1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I-2,
    L is J+1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I+2,
    L is J-1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I-2,
    L is J-1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I+1,
    L is J+2,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I-1,
    L is J+2,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I+1,
    L is J-2,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I-1,
    L is J-2,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).

within_board(I,J) :-
    between(1,9,I),
    between(1,9,J).
    
assignment(Assignments, I,J, cell(I,J)=V) :-
    within_board(I,J),
    member(cell(I,J)=V, Assignments).

all_different([cell(_,_)=V | Assignments]) :-
    findall(V, member(cell(_,_)=V, Assignments), EqualAssignments),
    length(EqualAssignments, 0),
    all_different(Assignments).
all_different([]).

all_distant_enough(cell(_,_)=_, []).
all_distant_enough(cell(_,_)=V, [cell(_,_)=U | Neighbours]) :-
    Diff is abs(V-U),
    Diff >= 2,
    all_distant_enough(cell(_,_)=V, Neighbours).

different_from(cell(_,_)=V, Cells) :-
    member(cell(_,_)=U, Cells),
    all_different([cell(_,_)=U,cell(_,_)=V]).

knights_move_valid(Assignments, I,J) :-
    member(cell(I,J)=V, Assignments),
    findall(cell(K,L)=U, knights_move(Assignments, I,J, cell(K,L)=U), MovesAssignments),
    findall(_, different_from(cell(I,J)=V, MovesAssignments), ValidMoves),
    length(ValidMoves, L1),
    length(MovesAssignments, L2),
    L1=L2.
kings_move_valid(Assignments, I,J) :-
    member(cell(I,J)=V, Assignments),
    findall(cell(K,L)=U, kings_move(Assignments, I,J, cell(K,L)=U), MovesAssignments),
    findall(_, different_from(cell(I,J)=V, MovesAssignments), ValidMoves),
    length(ValidMoves, L1),
    length(MovesAssignments, L2),
    L1=L2.
row_valid(Assignments, I,J) :-
    member(cell(I,J)=V, Assignments),
    findall(cell(K,L)=U, row_cell(Assignments, I,J, cell(K,L)=U), RowAssignments),
    all_different([cell(I,J)=V | RowAssignments]).
col_valid(Assignments, I,J) :-
    member(cell(I,J)=V, Assignments),
    findall(cell(K,L)=U, col_cell(Assignments, I,J, cell(K,L)=U), ColAssignments),
    all_different([cell(I,J)=V | ColAssignments]).
box_valid(Assignments, I,J) :-
    member(cell(I,J)=V, Assignments),
    findall(cell(K,L)=U, box_cell(Assignments, I,J, cell(K,L)=U), BoxAssignments),
    all_different([cell(I,J)=V | BoxAssignments]).
adjacent_cell_valid(Assignments, I,J) :-
    member(cell(I,J)=V, Assignments),
    findall(cell(K,L)=U, adjacent_cell(Assignments, I,J, cell(K,L)=U), AdjAssignments),
    all_distant_enough(cell(I,J)=V, AdjAssignments).

is_enough_valid_cells(Boxes, Rows, Cols, Kings, Knights, Adj) :-
    length(Boxes, L1),
    length(Rows, L2),
    length(Cols, L3),
    length(Kings, L4),
    length(Knights, L5),
    length(Adj, L6),
    L1 = 81,
    L2 = 81,
    L3 = 81,
    L4 = 81,
    L5 = 81,
    L6 = 81.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


encode_killer(killer(Hints), Map, Constraints) :-
    declareKillerVars(Hints,Map,VarsDeclarations),
    eachRowAllDifferent(Map,RowConstraints),
    eachColAllDifferent(Map,ColConstraints),
    eachBoxAllDifferent(Map,BoxConstraints),
    kingsMovesAllDifferent(Map,KingsConstraints),
    knightsMovesAllDifferent(Map,KnightsConstraints),
    adjacentDistantEnough(Map,AdjConstraints),
    append(VarsDeclarations,RowConstraints,T0),
    append(T0,ColConstraints,T1),
    append(T1,BoxConstraints,T2),
    append(T2,KingsConstraints,T3),
    append(T3,KnightsConstraints,T4),
    append(T4,AdjConstraints,Constraints).
    

declareKillerVars(Hints,Map,VarsDeclarations) :-
    declareKillerVars(1,1,Hints,Map,VarsDeclarations).
declareKillerVars(Row,Col,Hints,[cell(Row,Col)=Var|Map],[VarDecl|VarsDeclarations]) :-
    Row=<9, Col<9,
    (   % if the current cell is in the hints, the declaration requires the new variable to EQUAL the hint value, else it's between 1 and 9.
        member(cell(Row,Col)=Hint, Hints) ->
            VarDecl=new_int(Var,Hint,Hint) ;
            VarDecl=new_int(Var,1,9)
    ),
    Col1 is Col+1,
    declareKillerVars(Row,Col1,Hints,Map,VarsDeclarations).
declareKillerVars(Row,Col,Hints,[cell(Row,Col)=Var|Map],[VarDecl|VarsDeclarations]) :-
    Row=<9, Col=9,
    (   % if the current cell is in the hints, the declaration requires the new variable to EQUAL the hint value, else it's between 1 and 9.
        member(cell(Row,Col)=Hint, Hints) ->
            VarDecl=new_int(Var,Hint,Hint) ;
            VarDecl=new_int(Var,1,9)
    ),
    Col1 is 1,
    Row1 is Row+1,
    declareKillerVars(Row1,Col1,Hints,Map,VarsDeclarations).
declareKillerVars(Row,_,_,[],[]) :-
    Row>9.


eachRowAllDifferent(Map,RowsConstraints) :-
    eachRowAllDifferent(1,Map,RowsConstraints).
eachRowAllDifferent(Row,Map,[Constraint|RowsConstraints]) :-
    Row=<9,
    rowVariables(Row,Map,VarList),
    Constraint=int_array_allDiff(VarList),
    Row1 is Row+1,
    eachRowAllDifferent(Row1,Map,RowsConstraints).
eachRowAllDifferent(10,_, []).

rowVariables(Row,Map,Vars) :-
    rowVariables(Row,1,Map, Vars).
rowVariables(Row,Col,Map, [Var|Vars]) :-
    Col=<9,
    member(cell(Row,Col)=Var, Map),
    Col1 is Col+1,
    rowVariables(Row,Col1,Map,Vars).
rowVariables(_,10,_,[]).


eachColAllDifferent(Map,ColsConstraints) :-
    eachColAllDifferent(1,Map,ColsConstraints).
eachColAllDifferent(Col,Map,[Constraint|ColsConstraints]) :-
    Col=<9,
    colVariables(Col,Map,VarList),
    Constraint=int_array_allDiff(VarList),
    Col1 is Col+1,
    eachColAllDifferent(Col1,Map,ColsConstraints).
eachColAllDifferent(10,_, []).

colVariables(Col,Map,Vars) :-
    colVariables(Col,1,Map, Vars).
colVariables(Col,Row,Map, [Var|Vars]) :-
    Row=<9,
    member(cell(Row,Col)=Var, Map),
    Row1 is Row+1,
    colVariables(Col,Row1,Map,Vars).
colVariables(_,10,_,[]).

eachBoxAllDifferent(Map,BoxConstraints) :-
    findall((Row,Col), (between(0,2,I),between(0,2,J), Row is I*3+1, Col is J*3+1), Boxes),     % generate all boxes start col and start row.
    eachBoxAllDifferent(Boxes,Map,BoxConstraints).
eachBoxAllDifferent([],_,[]).
eachBoxAllDifferent([(StartRow,StartCol)|Boxes], Map,[CurrConstraints|BoxConstraints]) :-
    findall(
        (Row,Col),
        (
            between(0,2,RowOffset),
            between(0,2,ColOffset),
            Row is StartRow+RowOffset,
            Col is StartCol+ColOffset
        ),
        BoxCellsCoordinates
    ),
    varsFromCoordinates(BoxCellsCoordinates, Map,Vars),
    CurrConstraints=int_array_allDiff(Vars),
    eachBoxAllDifferent(Boxes,Map,BoxConstraints).

kingsMovesAllDifferent(Map,KingsConstraints) :-
    findall((I,J), (between(1,9,I),between(1,9,J)), Cells),
    kingsMovesAllDifferent(Cells, Map, KingsConstraints).
kingsMovesAllDifferent([], _, []).
kingsMovesAllDifferent([(I,J)|Cells], Map, Constraints) :-
    findall((Row,Col), kings_move(Map,I,J,cell(Row,Col)=_), KingsMovesCoords),
    varsFromCoordinates(KingsMovesCoords,Map, MovesVars),
    member(cell(I,J)=Var, Map),
    allDifferentFromVar(Var, MovesVars,CurrConstraints),
    append(CurrConstraints, NextConstraints, Constraints),
    kingsMovesAllDifferent(Cells,Map,NextConstraints).


knightsMovesAllDifferent(Map,KnightsConstraints) :-
    findall((I,J), (between(1,9,I),between(1,9,J)), Cells),
    knightsMovesAllDifferent(Cells, Map, KnightsConstraints).
knightsMovesAllDifferent([], _, []).
knightsMovesAllDifferent([(I,J)|Cells], Map, Constraints) :-
    findall((Row,Col), knights_move(Map,I,J,cell(Row,Col)=_), KnightsMovesCoords),
    varsFromCoordinates(KnightsMovesCoords,Map, MovesVars),
    member(cell(I,J)=Var, Map),
    allDifferentFromVar(Var, MovesVars,CurrConstraints),
    append(CurrConstraints, NextConstraints, Constraints),
    knightsMovesAllDifferent(Cells,Map,NextConstraints).

adjacentDistantEnough(Map,AdjConstraints) :-
    findall((I,J), (between(1,9,I),between(1,9,J)), Cells),
    adjacentMovesAllDifferent(Cells, Map, AdjConstraints).
adjacentMovesAllDifferent([], _, []).
adjacentMovesAllDifferent([(I,J)|Cells], Map, Constraints) :-
    findall((Row,Col), adjacent_cell(Map,I,J,cell(Row,Col)=_), AdjacentCoords),
    varsFromCoordinates(AdjacentCoords,Map, MovesVars),
    member(cell(I,J)=Var, Map),
    allDistantEnoughFromVar(Var, MovesVars,CurrConstraints),
    append(CurrConstraints, NextConstraints, Constraints),
    adjacentMovesAllDifferent(Cells,Map,NextConstraints).
    

varsFromCoordinates([], _,[]).
varsFromCoordinates([(I,J)|Coords], Map,[Var|Vars]) :-
    member(cell(I,J)=Var, Map),
    varsFromCoordinates(Coords, Map, Vars).


allDifferentFromVar(_,[],[]).
allDifferentFromVar(Var,[OtherVar|OtherVars],[CurrConstraints|Constraints]) :-
    CurrConstraints=int_array_allDiff([Var,OtherVar]),
    allDifferentFromVar(Var,OtherVars,Constraints).


allDistantEnoughFromVar(_,[], []).
allDistantEnoughFromVar(Var,[OtherVar|OtherVars], Constraints) :-
    %   we require that |Var-OtherVar|>=2 by requiring:
    %   Var-OtherVar >=2 OR OtherVar-Var>=2 which is equivalent to:
    %   Var-2>=OtherVar OR OtherVar-2>=Var
    CurrConstraints =[
        new_bool(X), new_bool(Y),
        new_int_plusK(Diff1,Var,-2),
        new_int_plusK(Diff2,OtherVar,-2),
        int_geq_reif(Diff1,OtherVar,X),
        int_geq_reif(Diff2,Var,Y),
        bool_array_or([X,Y])
    ],
    append(CurrConstraints, NextConstraints,Constraints),
    allDistantEnoughFromVar(Var,OtherVars,NextConstraints).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

decode_killer([],[]).
decode_killer([cell(I,J)=CellBits|Map],[cell(I,J)=CellValue|Solution]) :- 
    bDecode:decodeIntArray([CellBits],[CellValue]),
    decode_killer(Map,Solution).
