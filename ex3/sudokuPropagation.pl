is_propagation(sudoku(Dimension, Hints), I,J, Propagation) :-
    N is Dimension*Dimension,
    row_constraints(N, I, Hints, RowConstaints),
    col_constraints(N, J, Hints, ColConstaints),
    box_constraints(Dimension, I, J, Hints, BoxConstaints),
    append(RowConstaints, ColConstaints, RowColConstraints),
    append(RowColConstraints, BoxConstaints, AllConstraints),
    missing(N, AllConstraints, Missing),
    length(Missing, 1),
    head(Missing, Propagation).

%   succeed iff cell I,J cannot be deterministically propagated.
is_not_propagation(sudoku(Dimension, Hints), I,J) :-
    N is Dimension*Dimension,
    row_constraints(N, I, Hints, RowConstaints),
    col_constraints(N, J, Hints, ColConstaints),
    box_constraints(Dimension, I, J, Hints, BoxConstaints),
    append(RowConstaints, ColConstaints, RowColConstraints),
    append(RowColConstraints, BoxConstaints, AllConstraints),
    missing(N, AllConstraints, Missing),
    length(Missing, L),
    L>1.

head([H|_], H).

%   succeeds iff X is a valid value missing in an input list
notin(N,X,[C|List]) :-
    between(1,N,X),
    X =\= C,
    notin(N,X,List).
notin(N,X,[]) :-
    between(1,N,X).

%   calculates all valid values for cells, missing from "Constraints"
missing(N, Constraints, Missing) :-
    findall(X, notin(N,X,Constraints), Missing).

row_constraints(_, I, Hints, RowConstraints) :-
    findall(V, member(cell(I,_)=V, Hints), RowConstraints).

col_constraints(_, J, Hints, ColConstraints) :-
    findall(V, member(cell(_,J)=V, Hints), ColConstraints).

box_constraints(Dimension, I,J, Hints, BoxConstraints) :-
    cells_in_box(Dimension,I,J,Hints,Cells),
    get_cells_constraints(Cells, BoxConstraints).

%   calculates hints contained in the box of cell I,J.
cells_in_box(Dimension,I,J,Hints,BoxHints) :- 
    I1 is I-1,
    J1 is J-1,
    BoxI is div(I1,Dimension),
    BoxJ is div(J1,Dimension),
    BoxStartRow is (BoxI+1)*Dimension - Dimension+1,
    BoxStartCol is (BoxJ+1)*Dimension - Dimension+1,
    hints_in_box(Dimension, BoxStartRow, BoxStartCol, Hints, BoxHints).

%   calculates hints of box starting at Input row and col.
hints_in_box(Dimension, StartingRow, StartingCol, Hints, List) :-
    Ur is StartingRow + Dimension - 1,
    Uc is StartingCol + Dimension - 1,
    findall((I,J), (between(StartingRow, Ur, I), between(StartingCol, Uc, J)), Cells),
    findall(cell(I,J)=V, cell_is_in_hints(I,J,V,Cells,Hints), List).

%   generates I,J,V such that cell(I,J) is in Cells and in hints/
cell_is_in_hints(I,J,V,Cells,Hints) :- 
    member((I,J),Cells),
    member(cell(I,J)=V, Hints).


%   extracts only the value of cells in input
get_cells_constraints([cell(_,_)=C | Cells], [C | Constraints]) :-
    get_cells_constraints(Cells, Constraints).
get_cells_constraints([],[]).


%   generates cells to be checked for propagation.
propegatable_cell(sudoku(Dimension, Hints), I,J) :-
    N is Dimension*Dimension,
    between(1,N,I),
    between(1,N,J),
    \+ member(cell(I,J)=_, Hints).  % all cells which are not in Hints.

sudoku_propagate(sudoku(Dimension, Hints), List) :-
    findall(cell(I,J)=V, cell_propagation(sudoku(Dimension, Hints), I,J,V), CurrPropagations),
    CurrPropagations \= [],
    append(Hints, CurrPropagations, NewHints),
    sudoku_propagate(sudoku(Dimension, NewHints), NextPropagations),
    append(CurrPropagations, NextPropagations, List).
sudoku_propagate(sudoku(Dimension, Hints), List) :-
    findall(cell(I,J)=V, cell_propagation(sudoku(Dimension, Hints), I,J,V), CurrPropagations),
    CurrPropagations = [],
    List = CurrPropagations.

cell_propagation(sudoku(Dimension, Hints), I,J, V) :- 
    propegatable_cell(sudoku(Dimension, Hints), I,J),
    is_propagation(sudoku(Dimension, Hints), I,J,V).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

is_explanation(sudoku(Dimension, Hints), I,J, Explanation->[cell(I,J)=Propagation]) :-
    N is Dimension*Dimension,
    row_hints(N, I, Hints, RowHints),
    col_hints(N, J, Hints, ColHints),
    box_hints(Dimension, I, J, Hints, BoxHints),
    append(RowHints, ColHints, RowColHints),
    append(RowColHints, BoxHints, AllHints),
    findall(V, member(cell(_,_)=V, AllHints), ConstraintsValues),
    missing(N, ConstraintsValues, Missing),
    length(Missing, 1),
    explanation(AllHints, Explanation),
    head(Missing, Propagation).

%   creates the minimal explanation from the hints that correspond to a cell.
explanation([cell(_,_)=V|AllHints], [cell(_,_)=V|Explanation]) :-
    findall(V,member(cell(_,_)=V,AllHints), EqualConstraints),
    length(EqualConstraints, 0),
    explanation(AllHints, Explanation).
explanation([cell(_,_)=V|AllHints], Explanation) :-
    findall(V,member(cell(_,_)=V,AllHints), EqualConstraints),
    length(EqualConstraints, L),
    L>0,
    explanation(AllHints, Explanation).
explanation([],[]).

%   succeed iff cell I,J cannot be deterministically propagated.
is_not_explanation(sudoku(Dimension, Hints), I,J) :-
    N is Dimension*Dimension,
    row_hints(N, I, Hints, RowHints),
    col_hints(N, J, Hints, ColHints),
    box_hints(Dimension, I, J, Hints, BoxHints),
    append(RowHints, ColHints, RowColHints),
    append(RowColHints, BoxHints, AllHints),
    findall(V, member(cell(_,_)=V, AllHints), ConstraintsValues),
    missing(N, ConstraintsValues, Missing),
    length(Missing, L),
    L>1.

row_hints(_, I, Hints, RowHints) :-
    findall(cell(I,J)=V, member(cell(I,J)=V, Hints), RowHints).

col_hints(_, J, Hints, ColHints) :-
    findall(cell(I,J)=V, member(cell(I,J)=V, Hints), ColHints).

%   calculates hints contained in the box of cell I,J.
box_hints(Dimension, I,J, Hints, BoxHints) :-
    I1 is I-1,
    J1 is J-1,
    BoxI is div(I1,Dimension),
    BoxJ is div(J1,Dimension),
    BoxStartRow is (BoxI+1)*Dimension - Dimension+1,
    BoxStartCol is (BoxJ+1)*Dimension - Dimension+1,
    hints_in_box(Dimension, BoxStartRow, BoxStartCol, Hints, BoxHints).   

cell_explanation(sudoku(Dimension, Hints), I,J, Explanation) :- 
    propegatable_cell(sudoku(Dimension, Hints), I,J),
    is_explanation(sudoku(Dimension, Hints), I,J,Explanation).

sudoku_propagate_explain(sudoku(Dimension, Hints), Explain) :- 
    findall(E->[cell(I,J)=V], cell_explanation(sudoku(Dimension, Hints), I,J,E->[cell(I,J)=V]), CurrExplanations),
    CurrExplanations \= [],
    findall(cell(I,J)=V, member(_->[cell(I,J)=V],CurrExplanations), CurrHints),
    append(Hints, CurrHints, NewHints),
    sudoku_propagate_explain(sudoku(Dimension, NewHints), NextExplanations),
    append(CurrExplanations, NextExplanations, Explain).

sudoku_propagate_explain(sudoku(Dimension, Hints), Explain) :- 
    findall(E->[cell(I,J)=V], cell_explanation(sudoku(Dimension, Hints), I,J,E->[cell(I,J)=V]), CurrExplanations),
    CurrExplanations = [],
    Explain = [].



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
row_cell(Assignments, I,J, cell(I,L)=U) :-
    between(1,9,L),
    L \= J,
    member(cell(I,L)=U, Assignments).
col_cell(Assignments, I,J, cell(K,J)=U) :-
    between(1,9,K),
    K \= I,
    member(cell(K,J)=U, Assignments).
box_cell(Assignments, I,J, cell(K,L)=U) :-
    I1 is I-1,
    J1 is J-1,
    BoxI is div(I1,3),
    BoxJ is div(J1,3),
    BoxStartRow is (BoxI+1)*3 - 3+1,
    BoxStartCol is (BoxJ+1)*3 - 3+1,
    between(0,2, RowOffset),
    between(0,2, ColOffset),
    K is BoxStartRow + RowOffset,
    L is BoxStartCol + ColOffset,
    K\=I,
    L\=J,
    member(cell(K,L)=U, Assignments).

adjacent_cell(Assignments, I,J, cell(K,L)=V) :-
    K is I,
    L is J+1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
adjacent_cell(Assignments, I,J, cell(K,L)=V) :-
    K is I,
    L is J-1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
adjacent_cell(Assignments, I,J, cell(K,L)=V) :-
    K is I+1,
    L is J,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
adjacent_cell(Assignments, I,J, cell(K,L)=V) :-
    K is I-1,
    L is J,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).

kings_move(Assignments, I,J, cell(K,L)=V) :-
    adjacent_cell(Assignments,I,J,cell(K,L)=V).
kings_move(Assignments, I,J, cell(K,L)=V) :-
    K is I+1,
    L is J+1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
kings_move(Assignments, I,J, cell(K,L)=V) :-
    K is I+1,
    L is J-1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
kings_move(Assignments, I,J, cell(K,L)=V) :-
    K is I-1,
    L is J+1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
kings_move(Assignments, I,J, cell(K,L)=V) :-
    K is I-1,
    L is J-1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).

knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I+2,
    L is J+1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I-2,
    L is J+1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I+2,
    L is J-1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I-2,
    L is J-1,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I+1,
    L is J+2,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I-1,
    L is J+2,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I+1,
    L is J-2,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).
knights_move(Assignments, I,J, cell(K,L)=V) :-
    K is I-1,
    L is J-2,
    within_board(K,L),
    member(cell(K,L)=V,Assignments).

within_board(I,J) :-
    between(1,9,I),
    between(1,9,J).
    
assignment(Assignments, I,J, cell(I,J)=V) :-
    within_board(I,J),
    member(cell(I,J)=V, Assignments).

all_different([cell(_,_)=V | Assignments]) :-
    findall(V, member(cell(_,_)=V, Assignments), EqualAssignments),
    length(EqualAssignments, 0),
    all_different(Assignments).
all_different([]).

all_distant_enough(cell(_,_)=V, []).
all_distant_enough(cell(_,_)=V, [cell(_,_)=U | Neighbours]) :-
    Diff is abs(V-U),
    Diff >= 2,
    all_distant_enough(cell(_,_)=V, Neighbours).

different_from(cell(_,_)=V, Cells) :-
    member(cell(_,_)=U, Cells),
    all_different([cell(_,_)=U,cell(_,_)=V]).

knights_move_valid(Assignments, I,J) :-
    member(cell(I,J)=V, Assignments),
    findall(cell(K,L)=U, knights_move(Assignments, I,J, cell(K,L)=U), MovesAssignments),
    findall(_, different_from(cell(I,J)=V, MovesAssignments), ValidMoves),
    length(ValidMoves, L1),
    length(MovesAssignments, L2),
    L1=L2.
kings_move_valid(Assignments, I,J) :-
    member(cell(I,J)=V, Assignments),
    findall(cell(K,L)=U, kings_move(Assignments, I,J, cell(K,L)=U), MovesAssignments),
    findall(_, different_from(cell(I,J)=V, MovesAssignments), ValidMoves),
    length(ValidMoves, L1),
    length(MovesAssignments, L2),
    L1=L2.
row_valid(Assignments, I,J) :-
    member(cell(I,J)=V, Assignments),
    findall(cell(K,L)=U, row_cell(Assignments, I,J, cell(K,L)=U), RowAssignments),
    all_different([cell(I,J)=V | RowAssignments]).
col_valid(Assignments, I,J) :-
    member(cell(I,J)=V, Assignments),
    findall(cell(K,L)=U, col_cell(Assignments, I,J, cell(K,L)=U), ColAssignments),
    all_different([cell(I,J)=V | ColAssignments]).
box_valid(Assignments, I,J) :-
    member(cell(I,J)=V, Assignments),
    findall(cell(K,L)=U, box_cell(Assignments, I,J, cell(K,L)=U), BoxAssignments),
    all_different([cell(I,J)=V | BoxAssignments]).
adjacent_cell_valid(Assignments, I,J) :-
    member(cell(I,J)=V, Assignments),
    findall(cell(K,L)=U, adjacent_cell(Assignments, I,J, cell(K,L)=U), AdjAssignments),
    all_distant_enough(cell(I,J)=V, AdjAssignments).


verify_killer(killer(_),Solution,Verified) :-
    Solution=Assignments,
    findall(cell(I,J), box_valid(Assignments, I,J), CellsWithValidBoxes),
    findall(cell(I,J), row_valid(Assignments, I,J), CellsWithValidRows),
    findall(cell(I,J), col_valid(Assignments, I,J), CellsWithValidCols),
    findall(cell(I,J), kings_move_valid(Assignments, I,J), CellsWithValidKingsMoves),
    findall(cell(I,J), knights_move_valid(Assignments, I,J), CellsWithValidKnightsMoves),
    findall(cell(I,J), adjacent_cell_valid(Assignments, I,J), CellsWithValidAdjacentCells),
    is_enough_valid_cells(CellsWithValidBoxes, CellsWithValidRows, CellsWithValidCols, CellsWithValidKingsMoves, CellsWithValidKnightsMoves, CellsWithValidAdjacentCells),
    Verified=killer.
verify_killer(killer(_),Solution,Verified) :-
    Solution=Assignments,
    findall(cell(I,J), box_valid(Assignments, I,J), CellsWithValidBoxes),
    findall(cell(I,J), row_valid(Assignments, I,J), CellsWithValidRows),
    findall(cell(I,J), col_valid(Assignments, I,J), CellsWithValidCols),
    findall(cell(I,J), kings_move_valid(Assignments, I,J), CellsWithValidKingsMoves),
    findall(cell(I,J), knights_move_valid(Assignments, I,J), CellsWithValidKnightsMoves),
    findall(cell(I,J), adjacent_cell_valid(Assignments, I,J), CellsWithValidAdjacentCells),
    length(CellsWithValidBoxes, L1),
    length(CellsWithValidRows, L2),
    length(CellsWithValidCols, L3),
    length(CellsWithValidKingsMoves, L4),
    length(CellsWithValidKnightsMoves, L5),
    length(CellsWithValidAdjacentCells, L6),
    L1 = 81,
    L2 = 81,
    L3 = 81,
    L4 = 81,
    L5 = 81,
    L6 = 81,
    Verified=killer.

is_enough_valid_cells(Boxes, Rows, Cols, Kings, Knights, Adj) :-
    length(Boxes, L1),
    length(Rows, L2),
    length(Cols, L3),
    length(Kings, L4),
    length(Knights, L5),
    length(Adj, L6),
    L1 = 81,
    L2 = 81,
    L3 = 81,
    L4 = 81,
    L5 = 81,
    L6 = 81.

not_enough_valid_cells(ValidCells) :-
    length(ValidCells, L),
    L<81.

is_row_invalidation(Assignments, ValidCells, Invalidation) :-
    not_enough_valid_cells(ValidCells),
    head(ValidCells, cell(I,_)),    % get row number
    not_all_different(Assignments, cell(I,J)=V, cell(I,L)=U), !,
    Invalidation = [cell(I,J)=V, cell(I,L)=U].
is_col_invalidation(Assignments, ValidCells, Invalidation) :-
    not_enough_valid_cells(ValidCells),
    head(ValidCells, cell(_,J)),    % get col number
    not_all_different(Assignments, cell(I,J)=V, cell(K,J)=U), !,
    Invalidation = [cell(I,J)=V, cell(K,J)=U].
is_box_invalidation(Assignments, ValidCells, Invalidation) :-
    not_enough_valid_cells(ValidCells),
    head(ValidCells, cell(I,J)),    % get col number
    not_all_different(Assignments, cell(I,J)=V, cell(K,J)=U), !,
    Invalidation = [cell(I,J)=V, cell(K,J)=U].

not_all_different(Assignments, cell(I,J)=V, cell(K,L)=U) :-
    between(1,9,I),
    between(1,9,J),
    between(1,9,K),
    between(1,9,L),
    (I \= K ; J \= L),
    member(cell(I,J)=V, Assignments),
    member(cell(K,L)=U, Assignments),
    V=U.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

encode_killer(killer(Hints), Map, CNF) :-
    encode_cells(Hints, Map),
    encode_constraints(Map,CNF).

encode_cells(Hints, Map) :-
    allocate_variables(Map),
    unify_hints(Hints,Map).

allocate_variables(Map) :- 
    findall(
        cell(I,J)=[_1,_2,_3,_4,_5,_6,_7,_8,_9],
        (between(1,9,I),between(1,9,J)),
        Map    
    ).

unify_hints([],_).
unify_hints([cell(I,J)=V|Hints],Map) :-
    member(cell(I,J)=Vars, Map),
    to_unary(V, Vars),
    unify_hints(Hints,Map).

to_unary(N, [-1|Unary]) :-
    N>1,
    N1 is N-1,
    to_unary(N1, Unary).
to_unary(1, [1| Unary]) :-
    to_unary(0, Unary).
to_unary(0, [-1|Unary]) :-
    to_unary(0,Unary).
to_unary(_,[]).


encode_constraints(Map, Cnf) :-
    findall(I, between(1,9,I), Rows),
    findall(J, between(1,9,J), Cols),
    findall((Row,Col), (between(0,2,I),between(0,2,J), Row is I*3+1, Col is J*3+1), Boxes),
    findall((I,J), (between(1,9,I),between(1,9,J)), Cells),
    
    unary(Cells, Map, UnaryCnf),
    length(UnaryCnf, L1),
    row_all_different(Rows, Map, RowsCnf),
    length(RowsCnf, L2),
    col_all_different(Cols, Map, ColsCnf),
    length(ColsCnf, L3),
    box_all_different(Boxes, Map, BoxCnf),
    length(BoxCnf, L4),
    knights_all_different(Cells, Map, KnightsCnf),
    length(KnightsCnf, L5),
    kings_all_different(Cells, Map, KingsCnf),
    length(KingsCnf, L6),
    adjacent_all_different(Cells, Map, AdjCnf),
    length(AdjCnf,L7),
    concat_lists(UnaryCnf, RowsCnf, ColsCnf, BoxCnf, KnightsCnf, KingsCnf, AdjCnf, Cnf).
    % concat_lists(UnaryCnf, RowsCnf, ColsCnf, BoxCnf, KnightsCnf, KingsCnf, [], Cnf).


unary([], _, []).
unary([(I,J)|Coordinates], Map, Cnf) :-
    member(cell(I,J)=V, Map),
    unary_list(V, CurrCnf),
    unary(Coordinates, Map, CnfRest),
    append(CurrCnf, CnfRest, Cnf).

unary_list(Vars, Cnf) :-
    findall(
        (I,J),
        (between(1,9,I), between(1,9,J)),
        Pairs
    ),
    clauses_from_pairs(Pairs, Vars, CnfPairs),
    append([Vars],CnfPairs, Cnf).    % Vars is actually an "at least one is true" clause.

clauses_from_pairs([],_, []).
clauses_from_pairs([(I,J)|Pairs],Vars, Cnf) :-
    I =:= J,
    clauses_from_pairs(Pairs,Vars,Cnf).
clauses_from_pairs([(I,J)|Pairs],Vars, [Clause | Cnf]) :-
    I \= J,
    nth1(I,Vars, Var1),
    nth1(J,Vars, Var2),
    Clause = [-Var1, -Var2],
    clauses_from_pairs(Pairs, Vars, Cnf).

row_all_different([], _, []).
row_all_different([Row|Rows], Map, RowsCnf) :- 
    findall((Row,Col), member(cell(Row,Col)=_,Map), RowCells),
    all_different_clauses_from_coordinates(RowCells, Map, RowCnf),
    row_all_different(Rows,Map,RowsCnfRest),
    append(RowCnf, RowsCnfRest, RowsCnf).
col_all_different([], _, []).
col_all_different([Col|Cols], Map, ColsCnf) :- 
    findall((Row,Col), member(cell(Row,Col)=_,Map), ColCells),
    all_different_clauses_from_coordinates(ColCells, Map, ColCnf),
    col_all_different(Cols,Map,ColsCnfRest),
    append(ColCnf, ColsCnfRest, ColsCnf).
box_all_different([], _, []).
box_all_different([(I,J)|Boxes], Map, BoxesCnf) :- 
    findall(
        (Row,Col),
        (between(0,2,RowOffset),between(0,2,ColOffset), Row is I+RowOffset, Col is J+ColOffset),
        BoxCells
    ),
    all_different_clauses_from_coordinates(BoxCells,Map, BoxCnf),
    box_all_different(Boxes,Map,BoxesCnfRest),
    append(BoxCnf, BoxesCnfRest, BoxesCnf).
knights_all_different([], _, []).
knights_all_different([(I,J)|Cells], Map, KnightsCnf) :-
    findall(
        (Row,Col),
        knights_move(Map, I,J,cell(Row,Col)=_),
        KnightCells
    ),
    all_cells_different_from_cell_clauses((I,J),KnightCells,Map,KnightCnf),
    knights_all_different(Cells,Map,CnfRest),
    append(KnightCnf, CnfRest, KnightsCnf).
kings_all_different([], _, []).
kings_all_different([(I,J)|Cells], Map, KingsCnf) :-
    findall(
        (Row,Col),
        kings_move(Map, I,J,cell(Row,Col)=_),
        KingCells
    ),
    all_cells_different_from_cell_clauses((I,J),KingCells,Map,KingCnf),
    kings_all_different(Cells,Map,CnfRest),
    append(KingCnf, CnfRest, KingsCnf).
adjacent_all_different([], _,[]).
adjacent_all_different([(I,J)|Cells], Map,AdjCnf) :-
    findall(
        (Row,Col),
        adjacent_cell(Map, I,J,cell(Row,Col)=_),
        AdjCells
    ),
    all_cells_different_enough_from_cell_clauses((I,J), AdjCells, Map, CurrCnf),
    adjacent_all_different(Cells,Map,CnfRest),
    append(CurrCnf,CnfRest,AdjCnf).

  
all_different_clauses_from_coordinates(Coordinates, Map, Cnf) :-
    coordinates_to_values_variables(Coordinates,Map,Values),
    all_different(Values, Cnf).

all_cells_different_from_cell_clauses((I,J),Cells,Map,Cnf) :-
    member(cell(I,J)=V, Map),
    coordinates_to_values_variables(Cells, Map, Values),
    all_values_different_from_values(V, Values, Cnf).
all_values_different_from_values(_, [], []).
all_values_different_from_values(V, [Value|Values], Cnf) :-
    all_different([V,Value], CurrCnf),
    all_values_different_from_values(V,Values,CnfRest),
    append(CurrCnf, CnfRest, Cnf).

all_cells_different_enough_from_cell_clauses((I,J), Cells, Map, Cnf) :-
    member(cell(I,J)=V, Map),
    coordinates_to_values_variables(Cells, Map, Values),
    all_values_different_enough(V, Values,Cnf).
all_values_different_enough(_, [],[]).
all_values_different_enough(V, [Value|Values],Cnf) :-
    different_enough(V,Value,CurrCnf),
    all_values_different_enough(V,Values,CnfRest),
    append(CurrCnf, CnfRest, Cnf).

different_enough(V1,V2,Cnf) :-
    different_enough(1,V1,V2,Cnf).
different_enough(I,[B|V1], V2, [CurrCnf|Cnf]) :-
    I<10,
    only_two_distant_bit_can_be_true(I,B,V2,CurrCnf),
    I1 is I+1,
    different_enough(I1,V1,V2,Cnf).
    % append(CurrCnf,CnfRest,Cnf).
different_enough(10,_, _, []).

only_two_distant_bit_can_be_true(I,B,V2,Cnf) :-
    ILT is I-2,
    IGT is I+2,
    findall(J, between(1,ILT,J), IndicesLessThanI),
    findall(J, between(IGT,9,J), IndicesGreaterThanI),
    bits_of(IndicesLessThanI, V2, BitsBefore),
    bits_of(IndicesGreaterThanI, V2, BitsAfter),
    append([-B],BitsBefore, HalfCnf),
    append(HalfCnf, BitsAfter, Cnf).

bits_of([],_,[]).
bits_of([I|Indices], AllBits, [B|Bits]) :-
    nth1(I,AllBits,B),
    bits_of(Indices, AllBits, Bits).


coordinates_to_values_variables([(I,J)|Coordinates],Map,[V|Values]) :-
    member(cell(I,J)=V, Map),
    coordinates_to_values_variables(Coordinates, Map, Values).
coordinates_to_values_variables([], _, []).

all_different(Values, Cnf) :-
    all_different(1, Values, Cnf).
all_different(10, _, []).
all_different(N, Values, Cnf) :-
    N=<9,
    % nth_bit_is_unary(N, Values, CnfN),
    nth_bit_has_at_most_one_1(N,Values, CnfN),
    N1 is N+1,
    all_different(N1, Values, CnfRest),
    append(CnfN, CnfRest, Cnf).

nth_bit_has_at_most_one_1(N, Values, Cnf) :-
    length(Values, L),
    nth_bits(N,Values,Bits),
    findall(
        (I,J),
        (between(1,L,I), between(1,L,J)),
        Pairs
    ),
    clauses_from_pairs(Pairs, Bits, Cnf).

nth_bit_is_unary(N, Values, Cnf) :-
    nth_bits(N,Values,Bits),
    unary_list(Bits, Cnf).

nth_bits(_, [], []).
nth_bits(N, [V|Values], [B|Bits]) :-
    nth1(N,V,B),
    nth_bits(N,Values,Bits).

concat_lists(A,B,C,D,E,F,G,Concatination) :-
    append(A,B,T1),
    append(T1,C,T2),
    append(T2,D,T3),
    append(T3,E,T4),
    append(T4,F,T5),
    append(T5,G,Concatination).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

decode_killer(Map,Solution) :-
    findall(
        cell(I,J)=Value,
        (
            member(cell(I,J)=Unary, Map),
            decode_unary(Unary, Value)
        ),
        Solution
    ).

decode_unary(Unary, Value) :-
    decode_unary(1, Unary, Value).
decode_unary(N, [1|_], N) :-
    N =<9.
decode_unary(N, [-1|Unary], Value) :-
    N =<9,
    N1 is N+1,
    decode_unary(N1, Unary, Value).
decode_unary(10, _,0).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

legal_killer(killer(Hints), IsLegal) :- 
    encode_killer(killer(Hints), Map, Cnf),
    sat(Cnf),
    encode_killer(killer(Hints), NewMap, NewCnf),
    enforce_different_solution(Map, NewMap, DifferentCnf),
    append(NewCnf, DifferentCnf, CnfToProveThatAnotherSolutionExists),
    sat(CnfToProveThatAnotherSolutionExists) -> 
        getDifferentAssignment(Map,NewMap,IsLegal) ;
        IsLegal = legal.

enforce_different_solution(Map, NewMap, [Clause]) :-
    one_bit_different(Map,NewMap,Clause).

one_bit_different([cell(_,_)=V | Map],[cell(_,_)=Vars | NewMap],Clause) :-
    curr_cell_one_bit_different(V, Vars, CurrClause),
    one_bit_different(Map, NewMap, ClauseRest),
    append(CurrClause, ClauseRest, Clause).
one_bit_different([],[],[]).

curr_cell_one_bit_different([1|Bits], [V|Vars], [-V|Clause]) :-
    curr_cell_one_bit_different(Bits, Vars, Clause).
curr_cell_one_bit_different([-1|Bits], [V|Vars], [V|Clause]) :-
    curr_cell_one_bit_different(Bits, Vars, Clause).
curr_cell_one_bit_different([],[],[]).

getDifferentAssignment(Map,NewMap,IsLegal) :-
    decode_killer(Map,FirstSol),
    decode_killer(NewMap,SecondSol),
    findDifference(FirstSol, SecondSol, IsLegal).

findDifference([cell(I,J)=V|_], [cell(I,J)=U|_], IsLegal) :-
    V \= U,
    IsLegal =[cell(I,J)=V,cell(I,J)=U].
findDifference([cell(I,J)=V|Cells1], [cell(I,J)=U|Cells2], IsLegal) :-
    V =:= U,
    findDifference(Cells1, Cells2,IsLegal).

solve_killer(Instance, Solution) :-encode_killer(Instance,Map,Cnf),sat(Cnf),decode_killer(Map, Solution),verify_killer(Instance, Solution, Verified).