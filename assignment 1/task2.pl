nat(0).
nat(s(X)) :- nat(X).
lt(0,s(_)).
lt(s(X),s(Y)) :- lt(X,Y).
leq(0,_).
leq(s(X),s(Y)) :- leq(X,Y).
add(0, X, X). % :- nat(X).
add(s(X), Y, s(Z)) :- add(X,Y,Z).
times(0,_X,0).% :- nat(X).
times(s(X),Y,Z) :- lt(X,Z), leq(Y,Z), times(X,Y,W), add(W,Y,Z).

unary_divisor(N, K) :-
    times(K,R,N).

%   The predicate indeed works in mode unary_divisor(-,+).
unary_divisor_rev(N,K) :- unary_divisor(N,K).