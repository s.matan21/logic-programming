%   Written by: Matan Shaked

nat(0).
nat(s(X)) :- nat(X).
lt(0,s(_)).
lt(s(X),s(Y)) :- lt(X,Y).
leq(0,_).
leq(s(X),s(Y)) :- leq(X,Y).
add(0, X, X). % :- nat(X).
add(s(X), Y, s(Z)) :- add(X,Y,Z).
times(0,_X,0). % :- nat(X).
times(s(X),Y,Z) :- lt(X,Z), leq(Y,Z), times(X,Y,W), add(W,Y,Z).

%   Task 1
unary_sqrt(N,K) :- times(K,K, N).
% The predicate indeed works in the mode unary_sqrt(-,+)
% This is because in the 'times' predicate we limit the first argument
% to be lt the third one, which is giveb as an input, thus eventually Prolog
% would reach this upper bound and then return false, thus it will refrain 
% from an infinite loop.
unary_sqrt_rev(N,K) :- unary_sqrt(N,K).

%   Task 2
unary_divisor(N, K) :-
    times(K,_,N).
%   The predicate indeed works in mode unary_divisor(-,+).
%   The same explaination as in Task1.
unary_divisor_rev(N,K) :- unary_divisor(N,K).

%   First check that the last bit is 1.
%   Then check that the bits are only 1's and 0's.
is_binary(N) :- nonvar(N), reverse(N, R), is_binary_which_starts_with_one(R).

is_binary_which_starts_with_one([]).
is_binary_which_starts_with_one([X|T]) :- nonvar(X), X=1, only_ones_and_zeroes(T).

only_ones_and_zeroes([]).
only_ones_and_zeroes([X|T]) :- nonvar(X), is_bit(X), only_ones_and_zeroes(T).

is_bit(1).
is_bit(0).


%   Task 4

%   First we check that the inputs are indeed binary lists.
%   Then we perform the addition.
binary_plus(X,Y,Z) :- is_binary(X), is_binary(Y), add_bin(0,X,Y,Z).


%   The addition is given the curry, which starts at 0, the two operands and the output.
%   Each rule states the correct logic for the addition, while taking both the curry bit,
%   and the current bits of the operands in consideration.
add_bin(0,[0|Xr], [0|Yr], [0|Zr]) :- add_bin(0, Xr,Yr,Zr).
add_bin(0,[1|Xr], [0|Yr], [1|Zr]) :- add_bin(0, Xr,Yr,Zr).
add_bin(0,[0|Xr], [1|Yr], [1|Zr]) :- add_bin(0, Xr,Yr,Zr).
add_bin(0,[1|Xr], [1|Yr], [0|Zr]) :- add_bin(1, Xr,Yr,Zr).
add_bin(1,[0|Xr], [0|Yr], [1|Zr]) :- add_bin(0, Xr,Yr,Zr).
add_bin(1,[1|Xr], [0|Yr], [0|Zr]) :- add_bin(1, Xr,Yr,Zr).
add_bin(1,[0|Xr], [1|Yr], [0|Zr]) :- add_bin(1, Xr,Yr,Zr).
add_bin(1,[1|Xr], [1|Yr], [1|Zr]) :- add_bin(1, Xr,Yr,Zr).
%   Here we take care of the cases where only one operand has bits left to add.
add_bin(1, [X|Xr], [], Z) :- add_bin(0,[X|Xr],[1],Z).
add_bin(1, [], [Y|Yr], Z) :- add_bin(0,[1],[Y|Yr],Z).
add_bin(0, [X|Xr], [], [X|Xr]).
add_bin(0, [], [Y|Yr], [Y|Yr]).
%   Here we take care of the base case, which is just a curry value added to zeroes.
add_bin(1,[],[],[1]).
add_bin(0,[],[],[]).

%   This predicate only works in the mode bin_sum(+,+,-), This is because we use the predicate is_binary,
%   which enforces the input to not be a variable.
%   E.g., the statement: "bin_sum(X,Y,[1,1])." returns false.

%   For the predicate to work in the reverse mode, we define an alternative is_binary implementation,
%   which enables the use of variables:
is_binary_extended(N) :- reverse(N, R), is_binary_which_starts_with_one_extended(R).

is_binary_which_starts_with_one_extended([]).
is_binary_which_starts_with_one_extended([X|T]) :- X=1, only_ones_and_zeroes_extended(T).

only_ones_and_zeroes_extended([]).
only_ones_and_zeroes_extended([X|T]) :- is_bit(X), only_ones_and_zeroes_extended(T).

%   In addition, we define a generator for binary numbers of appropriate length.
%   I.e. the length of Z is at least the maximal length among X,Y, and at most the maximal length+1.
lengths_match([_|Xr],[_|Yr],[_|Zr]) :- lengths_match(Xr,Yr,Zr).
lengths_match([_|Xr],[],[_|Zr]) :- lengths_match(Xr,[],Zr).
lengths_match([],[_|Yr],[_|Zr]) :- lengths_match([],Yr,Zr).
lengths_match([],[],[]).
lengths_match([],[],[_]).

%   Now we define binary_plus_rev:
binary_plus_rev(X,Y,Z) :- 
    lengths_match(X,Y,Z),   % generate X,Y variables lists of appropriate length.
    is_binary_extended(X), is_binary_extended(Y),   % generate actual binary numbers from the variables lists.
    add_bin(0,X,Y,Z).   % perform addition.


%   Task 5
%   L is a binary palindrome if it is a binary number and it is equal to its reverse.
binary_palindrome(L) :- is_binary_extended(L), reverse(L,L).


%   Task 6
binary_palindrome_sum(N,A,B,C) :-
    binary_plus_rev(T,C,N), binary_plus_rev(A,B,T), % A+B=T and T+C=N meaning A+B+C=N
    binary_palindrome(A), binary_palindrome(B), binary_palindrome(C), % require A,B,C to be binary palindromes
    lte_binary(A,B), lte_binary(B,C). % require A<=B<=C.

%   X<=Y if X has less bits than Y and both are binary numbers.
lte_binary(X,Y) :- 
    length(X, Lx), length(Y,Ly), Lx<Ly,
    is_binary_extended(X), is_binary_extended(Y).
%   X<=Y if X has th same amount of bits as Y,
%   and each bit in X is equal to each bit in Y,
%   UNTIL one bit in X is < its corresponding bit in Y.
lte_binary(X,Y) :- 
    length(X, Lx), length(Y,Ly), Lx=Ly,
    is_binary_extended(X), is_binary_extended(Y),
    reverse(X,XR), reverse(Y,YR), lte_binary_equal_length(XR,YR).

lte_binary_equal_length([X|Xr],[X|Yr]) :- lte_binary_equal_length(Xr,Yr). % current bits are equal.
lte_binary_equal_length([0|_],[1|_]). % current bit in X is < current bit in Y
lte_binary_equal_length([],_). % 0  is <= all binary numbers.


%   Task 7
%   We use a naive method which checks:
%   forall 1<n<=sqrt(input) . n is not a divisor of input.
is_prime(3). % since sqrt(3)<2 we hard code it as a prime.
is_prime(X) :- is_prime_aux(2,X). % start testing divisablity for integers starting from 2.

%   base case - D is the potential divisor and it's >=sqrt(X). 
%   check if D is not divisor of X.
is_prime_aux(D,X) :-
    D >= sqrt(X),
    M is mod(X,D),
    M>0.
%   "induction step" - D is the potential divisor and it's < sqrt(X).
is_prime_aux(D,X) :- 
    D<sqrt(X),
    M is mod(X,D),
    M>0, %   check if D is not divisor of X.
    D1 is D+1,
    is_prime_aux(D1,X). % check if all integers >= D+1 are not divisors of X.


%   Task 8
%   base cases - all one digit primes are right truncatable primes.
right_prime(2).
right_prime(3).
right_prime(5).
right_prime(7).
%   "induction step" - N is prime and removing the least significant
%   digit of N (N/10) retains primality
right_prime(N) :- is_prime(N), Np is div(N,10), right_prime(Np).

%   This predicate does not work in the mode (-), since it is using is_prime
%   which only works in (+) mode.
%   is_prime only works in (+) mode because it is using Prolog arithmetics which 
%   require arguments to be sufficiently instantiated, i.e. they cannot be free variables.

%   We use a property of right truncatable primes which states that each digit after the leftmost one 
%   has to be either 1,3,7,9 (more on that in the PDF).
%   So we generate numbers whose first digit is a prime number and all others are in {1,3,7,9}, 
%   then we test if the numbers are right truncatable primes.
right_prime_gen(N) :- is_single_digit_prime(S), right_prime_candidate(S,N,73939133), right_prime(N).

is_single_digit_prime(2).
is_single_digit_prime(3).
is_single_digit_prime(5).
is_single_digit_prime(7).

%   This predicate generates a range of numbers consisting of the above described digits.
%   The first argument is the lower bound.
%   The second argument is the output.
%   The third argument is the upper bound.
right_prime_candidate(A,A,_).
right_prime_candidate(A,X,C) :- append_possible_digit(A,Ap), Ap=<C, right_prime_candidate(Ap,X,C).

append_possible_digit(A,B):- appendable_digit(D), B is A*10+D.

appendable_digit(1).
appendable_digit(3).
appendable_digit(7).
appendable_digit(9).