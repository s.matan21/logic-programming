% nat(N) :- N is a natural number

nat(0).
nat(s(X)) :- nat(X).

% ?- nat(s(s(s(0)))).
% ?- nat(s(s(s(X)))).
% ?- nat(s(s(s(a)))).
% ?- nat(elephant).

%-----------------------------------------

% add(X,Y,Z) :-
%    X,Y,Z are natural numbers 
%    such that  Z = X+Y

add(0, X, X). % :- nat(X).
add(s(X), Y, s(Z)) :- add(X,Y,Z).

% ?- add(s(0),s(0),X).      % 1+1=X
% ?- add(X,Y,s(s(0))).      % X+Y=2 
% ?- add(X,s(s(0)),Y).      % Y=X+2
% ?- add(s(0),s(0),s(0)).   % 1+1=1

% can we remove "nat(X)"
% what about ?- add(0,elephant,X).

% leq(X,Y) :-
%   X,Y are natural numbers 
%    such that X is less or equal than Y

leq(0,_).
leq(s(X),s(Y)) :- leq(X,Y).

% alternative
leq2(X,Y) :- add(X,_,Y).

% lt(X,Y) :-
%   X,Y are natural numbers 
%    such that X is less than Y

lt(0,s(_)).
lt(s(X),s(Y)) :- lt(X,Y).

% alternative
lt2(X,Y) :- add(X,s(_),Y).

%-----------------------------------------

% times(X,Y,Z) :-
%    X,Y,Z are natural numbers 
%    such that  Z = X*Y

times(0,_X,0).% :- nat(X).
times(s(X),Y,Z) :-   lt(X,Z), leq(Y,Z), times(X,Y,W), add(W,Y,Z).

% ?- times(s(s(0)),s(s(0)),Z).
% ?- times(X,Y,s(s(s(s(0))))). 
  %     X=1, Y=4 ; X=2, Y=2 ; loop
% ?- times(s(s(0)),X,s(s(s(0)))).
  %     loop