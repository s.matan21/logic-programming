is_binary([]).
is_binary([0|T]) :- is_binary(T), length(T, N), N>1.
is_binary([1|T]) :- is_binary(T), length(T, N), N>1.
is_binary([0|T]) :- length(T, N), N=1, T=[1].
is_binary([1|T]) :- length(T, N), N=1, T=[1].