test(0).
test(X) :- X=<10, R is X-1, test(R).


test1([_|T]).

test2([X|Xr], [], [X|Xr]).
test2([], [Y|Yr], [Y|Yr]).
test2([],[],[C]).


nat(0).
nat(s(X)) :- nat(X).
lt(0,s(_)).
lt(s(X),s(Y)) :- lt(X,Y).
leq(0,_).
leq(s(X),s(Y)) :- leq(X,Y).
add(0, X, X). % :- nat(X).
add(s(X), Y, s(Z)) :- add(X,Y,Z).
times(0,_X,0).% :- nat(X).
times(s(X),Y,Z) :- times(X,Y,W), add(W,Y,Z).
