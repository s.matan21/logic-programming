// Written by: Matan Shaked.

const fs = require("fs");

//  This function converts the string contained in the input file to a graph representation.
const createGraph = (graphStr) => {
    const lines = graphStr.split(/\n/);
    const graphSummary = lines.filter(line => line[0] == 'p');
    const edgesStr = lines.filter(line => line[0] == 'e');

    const [numOfVertices, numOfEdges] = graphSummary[0].match(/\d+/g).map(Number);
    const graph = Array.from({ length: numOfVertices }).map(_ => []);

    edgesStr
        .map(s => s.match(/\d+/g))
        .map(match => match.map(v => Number(v) - 1))
        .forEach(edge => {
            if (edge === undefined) {
                throw new Error("Failed finding vertices in an edge line!");
            }
            const [v1, v2] = edge;
            graph[v1].push(v2);
            graph[v2].push(v1);
        });

    return graph;
}

//  This is the method for calculating the hamiltonian cycle.
const calcHamCycle = (graph) => {
    const firstVertex = Number(Object.keys(graph)[0]);
    return isHamCycleAux(Object.keys(graph).length, graph, [firstVertex]);
}

//  This is an auxilary recursive function which builds a path and states if it is a hamiltonian cycle.
const isHamCycleAux = (numOfVertices, graph, currPath) => {

    const currVertex = currPath[currPath.length - 1];
    const possibleNexts = graph[currVertex];
    if (currPath.length == numOfVertices + 1) {

        return {
            isHamCycle: currVertex == currPath[0],
            path: currPath
        };
    }

    let hamCycle = { isHamCycle: false, path: [] };
    for (let i = 0; i < possibleNexts.length && !(hamCycle.isHamCycle); i++) {
        const nextVertex = possibleNexts[i];
        if (currPath.length >= numOfVertices || !currPath.includes(nextVertex)) {
            hamCycle = isHamCycleAux(numOfVertices, graph, [...currPath, nextVertex]);
        }
    }
    return hamCycle;
}

const argument = process.argv.slice(2)[0];
if (argument) {
    const filePath = argument;
    console.log("Calculating ham cycle for file: " + filePath);

    const file = fs.readFileSync(filePath);
    const fileContents = file.toString();
    const graph = createGraph(fileContents)

    console.time("calculation duration");
    const cycle = calcHamCycle(graph);
    console.timeEnd("calculation duration");
    const correctedCycle = cycle.path.map(x => x + 1);

    const output = cycle.isHamCycle ? Array.from(correctedCycle).slice(0, -1) : [];
    output.push(0);
    const outputStr = output.reduce((acc, currV) => acc + `${currV} `, "").slice(0, -1);

    const outfileName = filePath.replace("sol","hamsol");
    fs.writeFileSync(outfileName, outputStr);
}
else {
    console.log("No input file path given...");
}




