"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var file;
var graph = createGraph(file);
// const vertices = Object.keys(graph);
// const permutations = createPermutations(vertices);
// const cycles = permutations.filter(isHamCycle(graph));
// console.log();
var calcHamCycle = function (graph) {
    var firstVertex = Object.keys(graph)[0];
    return isHamCycleAux(graph, firstVertex);
};
var isHamCycleAux = function (graph, currPath) {
    if (currPath == "12345") {
        console.log();
    }
    var currVertex = Number(currPath.substr(-1));
    var possibleNexts = graph[currVertex];
    var numOfVertices = Object.keys(graph).length;
    if (currPath.length == numOfVertices + 1) {
        return {
            isHamCycle: currVertex == Number(currPath.charAt(0)),
            path: currPath
        };
    }
    var hamCycles = possibleNexts.map(function (nextVertex) {
        var newPath = currPath.concat(nextVertex.toString());
        if (currPath.search(nextVertex.toString()) != -1 && currPath.length < numOfVertices) {
            return {
                isHamCycle: false,
                path: currPath
            };
        }
        return isHamCycleAux(graph, newPath);
    }).filter(function (optionalHamCycle) { return optionalHamCycle.isHamCycle; });
    if (hamCycles.length > 0) {
        return hamCycles[0];
    }
    return {
        isHamCycle: false,
        path: currPath
    };
};
var graph = {
    1: [2],
    2: [1, 3, 5],
    3: [2, 4],
    4: [3, 5],
    5: [2, 4]
};
var cycle = calcHamCycle(graph);
console.log("" + 2 + 33);
// const isHamCycle = graph => permutation => {
//     permutation.reduce((isValidPath, currVertex, currIndex) => {
//         if (currIndex == permutation.length - 1) {
//             return isValidPath;
//         }
//         const nextVertex = permutation[currIndex + 1];
//         const currVertexNeighbours = graph[currVertex];
//         const isCurrEdgeValid = Boolean(currVertexNeighbours.find(vertex => vertex == nextVertex));
//         return isValidPath && isCurrEdgeValid;
//     }, true);
// }
// const permutations = arr => {
//     if (arr.length < 2)
//         return [arr];
//     const rest = (arr, i) => arr.filter((_, idx) => idx != i);
//     return arr.map((num, i) => permutations(rest(arr, i)).map(a => [num, ...a])).reduce();
// };
// const createPermutations = vertices =>
//     vertices.reduce((permutations, _) =>
//         inflatePermutations(permutations, vertices)
//         , vertices.map(v => [v])
//     );
// const inflatePermutations = (currPerms, vertices) =>
//     currPerms.map(currPerm => {
//         const missingVertices = subtractArrays(vertices, currPerm);
//         if (missingVertices.length == 0) {
//             return [currPerm]
//         }
//         return missingVertices.map(vertex => [
//             ...currPerm, vertex
//         ]);
//     }).reduce((flattened, currPerms) => [
//         ...flattened, ...currPerms
//     ]);
// const subtractArrays = (arr1, arr2) =>
//     arr1.filter(x => !arr2.includes(x));
var createGraph = function () {
};
// const perms = createPermutations([1, 2, 3, 4, 5, 6, 7, 8])
// console.log();
//# sourceMappingURL=ham-cycle.js.map